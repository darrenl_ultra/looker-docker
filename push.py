import subprocess
import sys
from datetime import datetime as dt
import os

if __name__ == '__main__':
    _id, folder, kind = sys.argv[1:]
    today = dt.now().strftime('%Y-%m-%d')
    print('Beginning Push\n')

    # create folder if doesn't exist
    if today not in os.listdir('deployments'):
        os.mkdir(f'deployments/{today}')
        print('Folder Created')

    export = f'gzr {kind} cat {_id} --host ultramobilestaging.looker.com > deployments/{today}/{_id}.json'
    print('Export statement:', export)
    p = subprocess.Popen(export,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)

    stdout, _ = p.communicate()
    print('Exported File\n')

    _import = f'gzr {kind} import deployments/{today}/{_id}.json {folder} --host ultramobile.looker.com --force'
    print('Import statment:', _import)

    print('Ready for import? Y/N')
    ready = input()

    if ready == 'Y':
        print('Importing...')
        p = subprocess.Popen(_import,
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)

        stdout, _ = p.communicate()
        print('STDOUT message:\n' + stdout.decode('utf-8'))
    else:
        print('Script terminated')
