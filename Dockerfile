FROM ubuntu:20.04

MAINTAINER Darren Liu

ENV PATH="/root/miniconda3/bin:${PATH}"
ARG PATH="/root/miniconda3/bin:${PATH}"

# System packages, ruby
RUN apt-get update -q && \
    apt-get install -qy procps curl ca-certificates gnupg2 build-essential --no-install-recommends && apt-get clean
RUN apt-get -o Acquire::Check-Valid-Until=false -o Acquire::Check-Date=false update && apt-get install -y wget vim sudo git && rm -rf /var/lib/apt/lists/*

RUN curl -sSL https://rvm.io/mpapis.asc | gpg2 --import -
RUN curl -sSL https://rvm.io/pkuczynski.asc | gpg2 --import -

RUN curl -sSL https://get.rvm.io | bash -s stable

SHELL [ "/bin/bash", "-l", "-c" ]
RUN rvm install 2.7.7
RUN gem install gazer
RUN gem update gazer

# Python installation
RUN wget \
    https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && mkdir /root/.conda \
    && bash Miniconda3-latest-Linux-x86_64.sh -b \
    && rm -f Miniconda3-latest-Linux-x86_64.sh

# set workdir, copy creds over
WORKDIR /home/looker

COPY Dockerfile .netrc* /root/
RUN chmod -f 600 /root/.netrc || :

ENTRYPOINT ["/bin/bash", "-l", "-c"]