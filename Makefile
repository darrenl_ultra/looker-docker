SHELL = /usr/bin/env bash

PORT = 8888


.PHONY: shell
shell: image
	docker run \
		--mount type=bind,src=${shell pwd},dst=/home/looker \
		-p ${PORT}:${PORT} \
		--rm -it looker //bin/bash

.PHONY: image
image:
	docker build -t looker .