import os
from shutil import copyfile
import json

def main():
	print('Beginning credentials setup')
	config = json.load(open('config.json'))
	staging_url = config['staging_url']
	staging_client_id = config['staging_client_id']
	staging_client_secret = config['staging_client_secret']
	prod_url = config['prod_url']
	prod_client_id = config['prod_client_id']
	prod_client_secret = config['prod_client_secret']

	# looker.ini
	with open('looker.ini', 'w') as fh:
		staging_looker_creds = f'base_url=https://{staging_url}:19999\nclient_id={staging_client_id}\nclient_secret={staging_client_secret}'
		prod_looker_creds = f'base_url=https://{prod_url}:19999\nclient_id={prod_client_id}\nclient_secret={prod_client_secret}'
		fh.write(f'[Staging]\n{staging_looker_creds}\nverify_ssl=True\n\n[Prod]\n{prod_looker_creds}\nverify_ssl=True')

	# .netrc
	with open('.netrc', 'w') as fh:
		staging_netrc_creds = f'machine {staging_url}\nlogin {staging_client_id}\npassword {staging_client_secret}'
		prod_netrc_creds = f'machine {prod_url}\nlogin {prod_client_id}\npassword {prod_client_secret}'
		fh.write(staging_netrc_creds + '\n\n' + prod_netrc_creds)
	copyfile('.netrc', os.getenv('HOME') + '/.netrc')
	os.chmod(os.getenv('HOME') + '/.netrc', 0o600)
	print('Setup completed')
	return

if __name__ == '__main__':
	main()