# looker-docker #

Use this repository to create a local `Docker` image to run an ubuntu container with `python`, `gazer`, and `looker-sdk` all set up.

## Installation
1. Download [Git Bash](https://gitforwindows.org/). Make sure to select "Use Windows' default console window" when asked to choose between that and "MinTTY."
2. Download [Docker Desktop](https://www.docker.com/products/docker-desktop).
3. Install `make` on windows. This can be done by downloading [Chocolatey](https://chocolatey.org/install) then running `choco install make` in PowerShell.

## Usage
1. Clone this repository locally using `Git Bash`.
2. Start Docker Desktop, make sure it is running.
3. Navigate to the repository's directory and run `make shell` to build the docker image. Note: the first time this is run it will take a while, but it will be much faster each subsequent time. Once the script finishes running, a container will be opened.
4. If this is your first time creating the image, you will need to add credentials (generate API key from `looker`). Input your credentials into `config.json` then run `python creds.py`. The json will look something like this:
    ```
    {
        "staging_url": "ultramobilestaging.looker.com"
        "staging_client_id": "ASDFGHJKL12345"
        "staging_client_secret": "12345ASDFGHJKL",
        "prod_url": "ultramobile.looker.com"
        "prod_client_id": "QWERTYUIOP67890"
        "prod_client_secret": "67890QWERTYUIOP"
    }
    ```
    This script `creds.py` will create the necessary files for `gazer` (`.netrc`) and `looker-sdk` (`looker.ini`) to work. If you ever need to change credentials, just change the json and re-run `creds.py`.
5. To test if `gazer` works, run `gzr user me --host <instance url>`.
6. Once credentials are set up, to run a push request, run `python push.py <source_item_id> <destination_folder_id> <look/dashboard>`, where

    - `source_item_id`: ID of the item to be pushed from staging. Either dashboard or look ID from BI ticket.
    - `destination_folder_id`: ID of the destination folder in prod.
    - `look/dashboard`: whether the item is a `look` or a `dashboard`
    
    After the item is exported from staging, you will be prompted with a `Y/N` before the import to prod begins. `Y` will run the import statement, `N` will exit the script.

7. To exit the container, run `exit`.
